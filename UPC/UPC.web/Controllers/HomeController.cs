﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPC.web.ViewModels;
using UPC.web.Models;

namespace UPC.web.Controllers
{
    public class HomeController : Controller
    {
       
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        //ADMIN
        [HttpGet]
        public ActionResult Dashboard_ADM()
        {
            Dashboard_ADMViewModel objLista = new Dashboard_ADMViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarEmpl_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarSup_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarPro_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult ListarAdm_ADM()
        {
            ListarTrabajadoresViewModel objLista = new ListarTrabajadoresViewModel();
            return View(objLista);
        }
        [HttpGet]
        public ActionResult AddEditTrabajador(int? UsuarioId)
        {
            AddEditTrabajdorViewModel objAddEdit = new AddEditTrabajdorViewModel();
            objAddEdit.CargarDatos(UsuarioId);     
            return View(objAddEdit);
        }
        [HttpPost]
        public ActionResult AddEditTrabajador(AddEditTrabajdorViewModel objViewModel)
        {
           try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario objUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == objViewModel.UsuarioId);
                if(objUsuario == null)
                {
                    objUsuario = new Usuario();
                    context.Usuario.Add(objUsuario);                   
                }     
                objUsuario.usuarioID = objViewModel.UsuarioId.Value;                           
                objUsuario.nombre = objViewModel.Nombre;
                objUsuario.apellidos = objViewModel.Apellidos;
                objUsuario.contraseña = objViewModel.Password;
                objUsuario.fecha_Nacimiento = objViewModel.fecha_nacimiento;
                objUsuario.estado = "ACT";
                objUsuario.localID = objViewModel.LocalId;
                objUsuario.categoriaID = objViewModel.CategoriaId;

                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                
                switch (objUsuario.categoriaID)
                {
                    case 1:
                        return RedirectToAction("ListarEmpl_ADM");
                    case 2:
                        return RedirectToAction("ListarSup_ADM");
                    case 3:
                        return RedirectToAction("ListarPro_ADM");                   
                    default:
                        return View(objViewModel);
                }             
            }
            catch(Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return View(objViewModel);
            }
        }
        [HttpGet]
        public ActionResult EliminarUsuario(int? UsuarioId)
        {
            try
            {
                TP_solucion4Entities context = new TP_solucion4Entities();
                Usuario ObjUsuario = context.Usuario.FirstOrDefault(x => x.usuarioID == UsuarioId);
                ObjUsuario.estado = "INA";
                context.SaveChanges();
                TempData["Mensaje"] = "Éxito! La operación se ejecuto exitosamente";
                switch (ObjUsuario.categoriaID)
                {
                    case 1:
                        return RedirectToAction("ListarEmpl_ADM");
                    case 2:
                        return RedirectToAction("ListarSup_ADM");
                    case 3:
                        return RedirectToAction("ListarPro_ADM");                   
                    default:
                        return View();
                }                       
            }
            catch (Exception ex)
            {
                TempData["Mensaje"] = "Error!" + ex.Message.ToList();
                return RedirectToAction("Dashboard_ADM");
            }
        }
        //EMPLEADOS
        public ActionResult Dashboard_EMP()
        {
                 
            Dashboard_EMPViewModel objViewModel_EMP = new Dashboard_EMPViewModel();
            return View(objViewModel_EMP);
        }

        //SUPERVISOR
        public ActionResult Dashboard_SUP()
        {

            Dashboard_SUPViewModel objViewModel_SUP = new Dashboard_SUPViewModel();
            return View(objViewModel_SUP);
        }

        //PROOVEDOR
        public ActionResult Dashboard_PRO()
        {

            Dashboard_PROViewModel objViewModel_PRO = new Dashboard_PROViewModel();
            return View(objViewModel_PRO);
        }

        //LOGIN Y CERRAR SESION
        public ActionResult Login()
        {
            LoginViewModel objViewModel = new LoginViewModel();
            return View(objViewModel);
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel objViewModel)
        {
            TP_solucion4Entities contex = new TP_solucion4Entities();
            Usuario objUsuario = contex.Usuario.FirstOrDefault(x => x.usuarioID == objViewModel.codigo && x.contraseña == objViewModel.password && x.estado == "ACT");          

            if (objUsuario == null)
            {                
                TempData["Mensaje"] = "Error! usuario y/o contraseña incorrectas";
                return View(objViewModel);
            }
            else
            {
                Categoria objCategoria = contex.Categoria.FirstOrDefault(x => x.categoriaID == objUsuario.categoriaID);
                objUsuario.estado = "LOG";
                contex.SaveChanges();
                Session["objUsuario"] = objUsuario;
                Session["objCategoria"] = objCategoria;
                switch (objUsuario.categoriaID)
                {
                    case 1:
                        return RedirectToAction("Dashboard_EMP"); 
                    case 2:
                        return RedirectToAction("Dashboard_SUP"); 
                    case 3:
                        return RedirectToAction("Dashboard_PRO"); 
                    case 4:
                        return RedirectToAction("Dashboard_ADM");
                    default:
                        return View(objViewModel);
                }


                                     
            }            
        }
        public ActionResult CerrarSesion()
        {
            TP_solucion4Entities contex = new TP_solucion4Entities();
            Usuario objUsuario = contex.Usuario.FirstOrDefault(x => x.estado=="LOG");
            objUsuario.estado = "ACT";
            contex.SaveChanges();
            Session.Clear();
            return RedirectToAction("Login");
        }


    }
}