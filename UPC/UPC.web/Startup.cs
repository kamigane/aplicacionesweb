﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UPC.web.Startup))]
namespace UPC.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
