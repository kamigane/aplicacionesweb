﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class Dashboard_EMPViewModel
    {
        public int nPuntos { get; set; }
        public int nComunicacion_INT { get; set; }
        public int nComunicacion_EXT { get; set; }

        public Dashboard_EMPViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuario objusuario = context.Usuario.FirstOrDefault(x => x.estado == "LOG");
            nPuntos = context.Informe_labor.Where(x=>x.persona_calificadaID==objusuario.usuarioID).Sum(x=>x.puntos);
            nComunicacion_INT = context.Anuncio.Where(x => x.usuarioID == objusuario.usuarioID).Count();
            nComunicacion_EXT = context.Local_evento.Count();
        }
    }
}