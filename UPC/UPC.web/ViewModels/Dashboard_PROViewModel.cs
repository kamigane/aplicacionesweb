﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class Dashboard_PROViewModel
    {
        public int nUsuariosEmpleados { get; set; }
        public int nPerspectiva { get; set; }
        public Dashboard_PROViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            Usuario objusuario = context.Usuario.FirstOrDefault(x => x.estado == "LOG");
            nPerspectiva = context.Informe360.Where(x=>x.usuarioID==objusuario.usuarioID).Count();
            nUsuariosEmpleados = context.Usuario.Where(x => x.categoriaID == 1).Count();
        }
    }
}