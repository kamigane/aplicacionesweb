﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPC.web.Models;
namespace UPC.web.ViewModels
{
    public class Dashboard_ADMViewModel
    {
        public int nEmpleados { get; set; }
        public int nSupervisores { get; set; }
        public int nProveedores { get; set; }
        public int nAdmins { get; set; }

        public Dashboard_ADMViewModel()
        {
            TP_solucion4Entities context = new TP_solucion4Entities();
            nEmpleados = context.Usuario.Where(x => x.categoriaID == 1).Count();
            nSupervisores = context.Usuario.Where(x => x.categoriaID == 2).Count();
            nProveedores = context.Usuario.Where(x => x.categoriaID == 3).Count();
            nAdmins = context.Usuario.Where(x => x.categoriaID == 4).Count();            
        }
    }
}